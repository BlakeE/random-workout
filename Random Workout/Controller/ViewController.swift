//
//  ViewController.swift
//  Random Workout
//
//  Created by Blake Eram on 2019-01-09.
//  Copyright © 2019 BlakeEram. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    var amountOfWorkouts: Int = 0
    
    
    @IBOutlet weak var exerciseNumberLabel: UILabel!
    
    @IBOutlet weak var stepper: UIStepper!
    override func viewDidLoad() {
        super.viewDidLoad()
        stepper.wraps = true
        stepper.autorepeat = true
        stepper.maximumValue = 10
               
    }

    @IBAction func increaseExerciseNumberStepper(_ sender: UIStepper) {
        
        exerciseNumberLabel.text =  "\(Int(stepper.value))"
        amountOfWorkouts = Int(stepper.value)
    }
    @IBAction func letsStartButtonPressed(_ sender: UIButton) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if amountOfWorkouts == 0 {
            let alert = UIAlertController(title: "Need at least 1", message: "Make sure to do at least one exercise or you are not really exercising", preferredStyle: .alert)
            let action = UIAlertAction(title: "You Ready?", style: .default) { (UIAlertAction) in
                return
            }
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        } else{
            let recieverVC = segue.destination as! WorkoutControllerViewController
            recieverVC.numberOfWorkouts = Int(stepper.value)
        }
        
    }
    
}

