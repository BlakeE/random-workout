//
//  WorkoutControllerViewController.swift
//  Random Workout
//
//  Created by Blake Eram on 2019-01-10.
//  Copyright © 2019 BlakeEram. All rights reserved.
//

import UIKit
import GoogleMobileAds

class WorkoutControllerViewController: UIViewController {
    
    let allWorkouts = WorkoutBank()
    var workoutNumber : Int = 1
    var numberOfWorkouts: Int = 0
    var interstitial: GADInterstitial!
    var goingToMainMenu = false
    
    

    @IBOutlet weak var currentWorkoutNumberLabel: UILabel!
    @IBOutlet weak var totalWorkoutNumberLabel: UILabel!
    
    @IBOutlet weak var currentWorkoutLabel: UIButton!
    @IBOutlet weak var currentIntensityLabel: UILabel!
    @IBOutlet weak var currentWorkoutTimeLabrl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-6578989924691026/4797128392")
        let request = GADRequest()
        interstitial.load(request)
        
       let workout = allWorkouts.workoutList[workoutNumber]
        currentWorkoutLabel.setTitle(workout.workoutName, for: .normal)
        currentIntensityLabel.text = workout.workoutReps
        currentWorkoutTimeLabrl.text = workout.workoutSets
        currentWorkoutNumberLabel.text = String(workoutNumber)
        totalWorkoutNumberLabel.text = String(numberOfWorkouts)
    }
    
    @IBAction func previousWorkoutButton(_ sender: Any) {
        
        if workoutNumber > 1 {
            workoutNumber -= 1
            let nextWorkout = allWorkouts.workoutList[workoutNumber]
            currentWorkoutLabel.setTitle(nextWorkout.workoutName, for: .normal)
            currentIntensityLabel.text = nextWorkout.workoutReps
            currentWorkoutTimeLabrl.text = nextWorkout.workoutSets
            currentWorkoutNumberLabel.text = String(workoutNumber)
        }
        
    }
    
    @IBAction func nextWorkoutButton(_ sender: Any) {
        if workoutNumber < numberOfWorkouts {
            workoutNumber += 1
            let nextWorkout = allWorkouts.workoutList[workoutNumber]
            currentWorkoutLabel.setTitle(nextWorkout.workoutName, for: .normal)
            currentIntensityLabel.text = nextWorkout.workoutReps
            currentWorkoutTimeLabrl.text = nextWorkout.workoutSets
            currentWorkoutNumberLabel.text = String(workoutNumber)
        }
        else {
            goingToMainMenu = true
            if interstitial.isReady {
                interstitial.present(fromRootViewController: self)
            } else {
                print("Ad wasn't ready")
            }
            let alertController = UIAlertController(title: "Congratulations", message: "You finished your workout!", preferredStyle: .alert)
            let mainMenu = UIAlertAction(title: "Main Menu", style: .cancel, handler: { action in self.performSegue(withIdentifier: "returnToMainMenu", sender: self)})
            alertController.addAction(mainMenu)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func goToInstructionsController(_ sender: UIButton) {
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if goingToMainMenu == false {
            let recieverVC = segue.destination as! InstructionsViewController
            let workout = allWorkouts.workoutList[workoutNumber]
            recieverVC.currentExercise = workout.workoutName
        } else {
            return
        }
    }
    
    @IBAction func unwindToFirstVC(segue:UIStoryboardSegue) { }
    

    
   
    

}
