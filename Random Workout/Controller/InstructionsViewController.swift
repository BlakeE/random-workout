//
//  InstructionsViewController.swift
//  Random Workout
//
//  Created by Blake Eram on 2019-01-11.
//  Copyright © 2019 BlakeEram. All rights reserved.
//

import UIKit
import GoogleMobileAds

class InstructionsViewController: UIViewController {
    //test git
    
    var currentExercise: String = ""
    var bannerView: GADBannerView!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var workoutPicture: UIImageView!
    @IBOutlet weak var instructionText: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        
        addBannerViewToView(bannerView)
        bannerView.adUnitID = "ca-app-pub-6578989924691026/7167237872"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        
        titleLabel.text = currentExercise
        workoutPicture.image = UIImage(named: currentExercise)
        workoutInstructions()

        // Do any additional setup after loading the view.
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .bottom,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    
    func workoutInstructions(){
        if currentExercise == "Ab Crunches"{
            instructionText.text = "Lie on your back with your knees bent and feet flat on the floor, hip-width apart. Place your hands behind your head so your thumbs are behind your ears. Curl up and forward so that your head, neck, and shoulder blades lift off the floor."
        } else if currentExercise == "Arm Curl"{
            instructionText.text = "Hold a dumbbell in each hand and stand with your feet as wide apart as your hips. Curl both arms upward until they’re in front of your shoulders. Slowly lower the dumbbells back down."
        } else if currentExercise == "Bench Press"{
            instructionText.text = "Lie down on the bench, planting your feet on the ground. Grab the bar just outside of shoulder-width. Keep your elbows within a 45-degree angle as you lower the bar down toward your sternum. Drive the bar back up and over your face."
        } else if currentExercise == "Bodyweight Lunge"{
            instructionText.text = "Stand erect with your feet hip-width apart. Take a moderate-length step forward with one foot, descending to a point in which your rear knee approaches the floor without touching, maintaining your body's upright posture (Your front knee should bend about 90 degrees). From the bottom position, push back up from your forward foot, bringing it back beside the other."
        } else if currentExercise == "Calf Raise"{
            instructionText.text = "Stand on the edge of a step. Raise your heels a few inches above the edge of the step so that you’re on your tiptoes. Hold the position for a moment, and then lower your heels below the platform, feeling a stretch in your calf muscles."
        } else if currentExercise == "Chest Fly"{
            instructionText.text = "Lie down on a flat bench with a dumbbell on each hand resting on top of your thighs. Lift the dumbbells one at a time so you can hold them in front of you at shoulder width with the palms of your hands facing each other. Raise the dumbbells up like you're pressing them, but stop and hold just before you lock out. This will be your starting position. With a slight bend on your elbows in order to prevent stress at the biceps tendon, lower your arms out at both sides in a wide arc until you feel a stretch on your chest. Return your arms back to the starting position as you squeeze your chest muscles."
        } else if currentExercise == "Chin Ups"{
            instructionText.text = "Put your hands on the bar with your palms facing your body. Raise your body until your chin is above the bar. Lower yourself back down."
        } else if currentExercise == "Dip"{
            instructionText.text = "Let your arms hang down at your sides and grasp the handles on the dips bars on either side of your torso. Bend your elbows directly behind you. Pause, and then push your weight into your hands to straighten your arms all the way."
        } else if currentExercise == "Dumbbell Bench"{
            instructionText.text = "Lie on the bench with a dumbbell in each hand and your feet flat on the floor. Push the dumbbells up so that your arms are directly over your shoulders and your palms are up. Lower the dumbbells down and push the dumbbells back up."
        } else if currentExercise == "Lat Pull Down"{
            instructionText.text = "Grab the bar with the palms facing forward using the prescribed grip. As you have both arms extended in front of you holding the bar at the chosen grip width, bring your torso back around 30 degrees or so while creating a curvature on your lower back and sticking your chest out. As you breathe out, bring the bar down until it touches your upper chest by drawing the shoulders and the upper arms down and back. Slowly raise the bar back to the starting position when your arms are fully extended and the lats are fully stretched."
        } else if currentExercise == "Leg Curl"{
            instructionText.text = "Curl your legs up as far as possible without lifting the upper legs from the pad. Once you hit the fully contracted position, hold it for a second. As you inhale, bring the legs back to the initial position. Repeat for the recommended amount of repetitions."
        } else if currentExercise == "Pull Ups"{
            instructionText.text = "Grab the pullup bar with your palms down. Hang to the pullup-bar with straight arms and your legs off the floor. Pull yourself up by pulling your elbows down to the floor and go until your chin is above the bar. Go down slowly back to starting position."
        } else if currentExercise == "Push Ups"{
            instructionText.text = "Begin on your hands and knees with your hands underneath your shoulders but slightly wider than your shoulders. Come onto the balls of your feet and the heels of your hands, and then walk the feet back until you're in the plank position. Begin to bend your elbows, lowering your body in one solid piece down towards the floor. Push yourself back up."
        } else if currentExercise == "Shoulder Press"{
            instructionText.text = "While holding a dumbbell in each hand, sit on a military press bench or utility bench that has back support. Raise the dumbbells to shoulder height one at a time using your thighs to help propel them up into position. Push the dumbbells upward until they touch at the top. After a brief pause at the top contracted position, slowly lower the weights back down."
        } else if currentExercise == "Squat"{
            instructionText.text = "Plant your feet flat on the ground, toes slightly outward. Position the bar behind your head, with the weight on your upper back. Bend your knees and slowly lower your hips as if to sit on an invisible chair. Push straight back up, lifting your hips up and forward to lift toward starting position."
        } else if currentExercise == "Straight-Arm Pulldown"{
            instructionText.text = "Start by grabbing the wide bar from the top pulley of a pulldown machine and using a wider than shoulder-width pronated (palms down) grip. Bend your torso forward at the waist by around 30-degrees with your arms fully extended in front of you and a slight bend at the elbows. While keeping the arms straight, pull the bar down by contracting the lats until your hands are next to the side of the thighs. While keeping the arms straight, go back to the starting position."
        } else if currentExercise == "Tricep Extension"{
            instructionText.text = "Stand up with a dumbbell held by both hands. Your feet should be about shoulder width apart from each other. Slowly use both hands to grab the dumbbell and lift it over your head until both arms are fully extended. Keeping your upper arms close to your head with elbows in and perpendicular to the floor, lower the resistance in a semicircular motion behind your head until your forearms touch your biceps."
        } else if currentExercise == "Tricep Pulldown"{
            instructionText.text = "Attach a straight or angled bar to a high pulley and grab with an overhand grip (palms facing down) at shoulder width. Using the triceps, bring the bar down until it touches the front of your thighs and the arms are fully extended perpendicular to the floor. After a second hold at the contracted position, bring the bar slowly up to the starting point."
        } else if currentExercise == "Wide-Grip Chest Press"{
            instructionText.text = "Lie down on the bench, planting your feet on the ground. Grab the bar outside of shoulder-width. Keep your elbows within a 45-degree angle as you lower the bar down toward your sternum. Drive the bar back up and over your face."
        } else {
            instructionText.text = "Could not find anything"
        }
        
    }
    
    
    

    @IBAction func dismissVC(_ sender: Any) {
       performSegue(withIdentifier: "back", sender: self)
    }
    

}
