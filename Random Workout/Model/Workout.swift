//
//  Workout.swift
//  Random Workout
//
//  Created by Blake Eram on 2019-01-09.
//  Copyright © 2019 BlakeEram. All rights reserved.
//

import Foundation

class Workout {
    
    let workoutName: String
    let workoutReps: String
    let workoutSets: String
    
    init(workout: String, intensity: String, time: String){
        workoutName = workout
        workoutReps = intensity
        workoutSets = time
    }
}
