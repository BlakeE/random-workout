//
//  WorkoutBank.swift
//  Random Workout
//
//  Created by Blake Eram on 2019-01-09.
//  Copyright © 2019 BlakeEram. All rights reserved.
//

import Foundation

class WorkoutBank {
    
    var workoutList = [Workout]()
    var numberOfWorkouts = 10
    
    var workouts = ["Arm Curl", "Bench Press", "Squat", "Lat Pull Down", "Tricep Pulldown", "Push Ups", "Pull Ups", "Chin Ups", "Ab Crunches","Dumbbell Bench", "Chest Fly", "Wide-Grip Chest Press", "Leg Curl", "Bodyweight Lunge", "Calf Raise", "Shoulder Press", "Straight-Arm Pulldown", "Tricep Extension", "Dip" ]
    
    var reps = ["1 Reps", "2 Reps", "3 Reps", "4 Reps", "5 Reps", "6 Reps", "7 Reps", "8 Reps", "9 Reps",
                       "10 Reps", "11 Reps", "12 Reps", "13 Reps", "14 Reps", "15 Reps", "16 Reps"]
    
    var sets = ["1 Set", "2 Sets", "3 Sets", "4 Sets", "5 Sets"]
    
    init() {        
        
        for _ in 0 ... numberOfWorkouts {
            randomWorkout()
        }
       
    }
    
    func randomWorkout(){
        let workoutNumber = Int.random(in: 0..<workouts.count)
        let repsNumber = Int.random(in: 0..<reps.count)
        let setsNumber = Int.random(in: 0..<sets.count)
        
        workoutList.append(Workout(workout: workouts[workoutNumber], intensity: reps[repsNumber], time: sets[setsNumber]))

    
    }
}
